<?php
class Admin{
    public $adminID;
    public $password;
    
    public function set_admin_id($adminid){
        $this->adminID = $adminid;
    }
    
    public function get_admin_id(){
        return $this->adminID;
    }
    
    public function validate_admin_id($adminId){
        return preg_match("/[S]{1}\d{6}/",$adminId);
    }


    public function set_admin_password($password){
        $this->password = $password;
    }
    
    public function get_admin_password(){
        return $this->password;
    }
    
    public function validate_admin_password($password){
        return preg_filter("^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{8,}$", $password);
        
        /*** Min eight characters, at least one letter, one number and one special character: ***/
    }
          
}