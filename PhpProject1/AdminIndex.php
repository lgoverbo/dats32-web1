<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <h1>Student administration page:</h1>
        <div>
         <form action="" method="POST">
                <div>
                    <input type="button" onclick="document.getElementById('SeeStudents').scrollIntoView();" value="See All Members"/>
                </div>
             <div>
                 <input type="button" onclick="document.getElementById('UpdateStudent').scrollIntoView();" value="Update Member Info"/>
             </div>
             <div>
                 <input type="button" onclick="document.getElementById('AddStudent').scrollIntoView();" value="Add New Member"/>
             </div>
             <div>
                 <input type="button" onclick="document.getElementById('DelStudent').scrollIntoView();" value="Delete Member"/>
             </div>
            </form>
        </div>
        <div>
            
                <div id="SeeStudents">
                    <h2>See All Students</h2></br>
                </div>
                <form action="" method="post"  name="SeeStudent">
                    <div>
                        <button type="submit" class="button" id="seeAllStudents" name="seeAllStudents">See All Students</button> 
                    </div>
                </form>
            
            
            <div>
                <div id="AddStudent">
                    <h2>Add Student</h2></br>
                </div>
                <form action="" method="POST" name="AddStudent" onSubmit="validate_all()">
                    <div>
                        <label for="name">Name:</label>
                        <input type="text" class="form-control" id="nameStudent" name="nameStudent">
                    </div>
                    <div>
                        <label for="name">Email:</label>
                        <input type="text" class="form-control" id="emailStudent" name="emailStudent">
                    </div>
                    <div>
                        <label for="name">StudentID:</label>
                        <input type="text" class="form-control" id="idStudent" name="idStudent">
                    </div>
                    <div>
                        <label for="name">Program:</label>
                        <input type="text" class="form-control" id="programStudent" name="programStudent">
                    </div>
                    <div>
                        <button type="submit" class="button" id="submitStudent" name="submitStudent">Submit Student</button>
                    </div>
                </form>
            </div>
            <div>
                <div id="UpdateStudent">
                    <h2>Update Student</h2></br>
                </div>
                <form action="" method="post" name="UpdateStudent">
                    <div>
                        <label for="updateStudent">Choose either name or studentnumber</label></br>
                        <input type="text" class="form-control" id="findStudent" name="findStudent"></br>
                        <button type="submit" class="button" id="findUpdate" name="findUpdate">Find Student</button>
                    </div>
                    <div>
                        <label for="name">Name:</label>
                        <input type="text" class="form-control" id="updateName" name="updateName">
                    </div>
                    <div>
                        <label for="name">Email:</label>
                        <input type="text" class="form-control" id="updateEmailStudent" name="updateEmail">
                    </div>
                    <div>
                        <label for="name">StudentID:</label>
                        <input type="text" class="form-control" id="updateId" name="updateId">
                    </div>
                    <div>
                        <label for="name">Program:</label>
                        <input type="text" class="form-control" id="updateProgram" name="updateProgram">
                    </div>
                    <div>
                        <button type="submit" class="button" id="submitUpdate" name="submitUpdate">Submit Update</button>
                    </div>
                </form>
            </div>
            <div>
                <div id="DelStudent">
                    <h2>Delete Student</h2></br>
                </div>
                <div>
                     <form action="" method="post" name="DeleteStudent">
                         <div>
                         <label for="updateStudent">Choose either name or studentnumber:</label></br>
                        <input type="text" class="form-control" id="findStudent" name="findStudent">
                        <button type="button" class="button" id="deleteStudent" name="deleteStudent">Delete Student</button></br>
                         </div>
                     </form>
                </div>
            </div>
            
        </div>
        <?php
        include "DB_Admin.php";
        include "Person.php";
        include "Student.php";
        include "Errorhandler.php";
        include "Validate.php";
       
       /*** Show host info**/ 
        
       $host = gethostname();
       $ip = gethostbyname($host);
       echo $host."</br>";
       echo $ip."</br>";
       echo $db->host_info;
       
      /******** Add Student ********/
       /*
        if(isset($_POST["submitStudent"])){
            $person = new Person();
            $errorString = validate_and_set_person($person);
            if($errorString!=null){
                echo "Error validating <br/>";
                echo $feilString;
            }
            else{
                $student = new Student();
                $errorString = validate_and_set_student($student);
                if($errorString!=null){
                    echo "error In Validating:<br/>";
                    echo $errorString."<br/>";
                }
                else{
                    $newStudent = new DB_Admin();
                    $errorstring1 = $newStudent->addStudent($person, $student);
                }
                
            }
            if($feilString1!=null){
                echo "Feil i lagring av informajson til databasen <br/>";
                echo $feilString1;
            }
            else{
                echo "Sjekk informasjon på nytt";
            }
        }
        */
       if(isset($_POST["submitStudent"])){
           $person = new Person();
           $student = new Student();
           $newStudent = new DB_Admin();
           $newStudent->addStudent($person, $student);
       }
        /*** Show all Students ***/ 
        
        if (isset($_POST["seeAllStudents"])){
            $seeAll = new DB_Admin();
            $seeAll->showStudents();
        } 
        
        /*** Update Student ***/
        if(isset($_POST["findStudent"])){
            $findStudent = new DB_Admin();
            $findStudent->findStudent($_POST["findStudent"]);
        }
        if(isset($_POST["submitUpdate"])){
            $name = $_POST["updateName"];
            $email = $_POST["updateEmail"];
            $studentID = $_POST["updateId"];
            $program = $_POST["updateProgram"];
            
            $updateStudent = new DB_Admin();
            $updateStudent->updateStudent($name, $email, $studentID, $program);
        }
        /*** Delte Student ***/
        if(isset($_POST["deleteStudent"])){
            $deleteStudent = new DB_Admin();
            $deleteStudent->deleteStudent($_POST["findStudent"]);
        }
       
       
       ?>
    </body>
</html>
