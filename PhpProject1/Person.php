<?php
class Person{
    public $name;
    public $email;
    
    public function set_name($name){
        $this->name = $name;
    }
    
    public function get_name(){
        return $this->name;
    }
    
    public function validate_name($name){
        return preg_filter("/^[a-z ,.'-]+$/i", $name);
    }
    
    public function set_email($email){
        $this->email = $email;
    }
    
    public function get_email(){
        return $this->email;
    }
    
    public function validate_email($email){
        return filter_var($email, FILTER_VALIDATE_EMAIL) !== false;
    }
}

