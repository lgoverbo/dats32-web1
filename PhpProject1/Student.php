<?php
class Student{
    public $studentid;
    public $program;
    
    public function set_id($studentid){
        $this->studentid = $studentid;
    }
    
    public function get_id(){
        return $this->studentid;
    }
    
    public function validate_id($studentid){
        return preg_match("/[S]{1}\d{6}/",$studentid);
    }


    public function set_program($program){
        $this->program = $program;
    }
    
    public function get_program(){
        return $this->program;
    }
    
    public function validate_program($program){
        return preg_filter("/[A-Z]{3-4}[0-9]{4}", $program);
    }
          
}

